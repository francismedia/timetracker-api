const _ = require("lodash");
const jwt = require("jwt-simple");

const AuthController = {
  login: (data, callback) => {
    /* This is the hardcoded list of registered users */
    const users = [
      {
        "username": "test",
        "password": "test"
      }
    ];
    const payload = data.payload || '{}';
    const { username, password } = JSON.parse(payload);
    const user = _.find(users, { username, password });

    if (user) {
      const token = jwt.encode({ user: data.payload.username }, "secret");
      callback(200, { token });
    } else {
      callback(200, { success: false });
    }

  }
};

module.exports = AuthController;