const employeesService = require('../services/employeesService');

const EmployeesController = {
  list: (data, cb) => {
    employeesService.list(data, cb);
  },

  show: (data, cb) => {
    employeesService.show(data, cb);
  },

  create: (data, cb) => {
    const payload = data.payload || '{}';
    const { name, status } = JSON.parse(payload);
    params = {
      info: {
        name,
        status: status ? true : false,
        timesheet: []
      }
    };
    employeesService.create(params, cb);
  },

  update: (data, cb) => {
    let payload = data.payload || '{}';
    payload = JSON.parse(payload);
    params = {
      uri: {
        id: data.uri.id
      },
      info: {
        name: payload.name,
        status: payload.status,
        timesheet: payload.timesheet || [],
      }
    };
    employeesService.update(params, cb);
  },

  delete: (data, cb) => {
    employeesService.delete(data, cb);
  },
};

module.exports = EmployeesController;
