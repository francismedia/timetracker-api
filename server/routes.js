const employees = require('./controllers/employeesController');
const passport = require('passport');
const auth = require('./controllers/authController');

const routes = {
  login: (data, callback) => {
    const acceptableMethods = ['options','post'];
    if (acceptableMethods.indexOf(data.method) > -1) {
      auth.login(data, callback);
    } else {
      callback(405);
    }
  },

  getEmployees: (data, callback) => {
    passport.authenticate('jwt', { session: false });
    const acceptableMethods = ['options', 'get', 'put', 'delete'];
    if (acceptableMethods.indexOf(data.method) > -1) {
      routes.employeesRoutes[data.method](data, callback);
    } else {
      callback(405);
    }
  },

  employees: (data, callback) => {
    passport.authenticate('jwt', { session: false });
    const acceptableMethods = ['options', 'get', 'post'];
    if (acceptableMethods.indexOf(data.method) > -1) {
      if (data.method === 'get') employees.list(data, callback);
      if (data.method === 'post') employees.create(data, callback);
      if (data.method === 'options') callback(200);
    } else {
      callback(405);
    }
  },

  employeesRoutes: {
    get: (data, callback) => { employees.show(data, callback); },
    put: (data, callback) => { employees.update(data, callback); },
    delete: (data, callback) => { employees.delete(data, callback); },
    'options': (data, callback) => { callback(200); }
  },

  notFound: (data, callback) => {
    callback(404);
  },
};

module.exports = routes;
