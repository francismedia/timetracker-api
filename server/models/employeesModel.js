const fs = require('fs');
const path = require('path');

const EmployeesModel = {
  baseDir: path.join(__dirname, '../../.data/'),

  list: (model, callback) => {
    fs.readdir(`${EmployeesModel.baseDir}/${model}/`, (error, employees) => {
      if (!error) {
        const getEmployees = new Promise((resolve, reject) => {
          const list = [];
          try {
            employees.map((filename) => {
              const employee = JSON.parse(fs.readFileSync(`${EmployeesModel.baseDir}/${model}/${filename}`, 'utf-8'));
              employee.id = filename.replace('.json', '');
              list.push(employee);
            });
          } catch (e) {
            reject(e);
          }
          resolve(list);
        });

        getEmployees
          .then((employees) => {
            callback(employees);
          }).catch((error) => {
            callback(error);
          });

      } else {
        callback(error);
      }
    });
  },

  create: (model, file, employee, callback) => {
    fs.open(`${EmployeesModel.baseDir}${model}/${file}.json`, 'wx', (error, fileDescriptor) => {
      if (!error && fileDescriptor) {
        const stringData = employee;
        fs.writeFile(fileDescriptor, stringData, (error) => {
          if (!error) {
            fs.close(fileDescriptor, (error) => {
              if (!error) {
                callback(false);
              } else {
                callback('Error closing new file');
              }
            });
          } else {
            callback('Error writing to new file');
          }
        });
      } else {
        callback('Cound not create new file, it may already exist');
      }
    });
  },

  read: (model, file, callback) => {
    fs.readFile(`${EmployeesModel.baseDir}${model}/${file}.json`, 'utf-8', (error, employee) => {
      if (!error) {
        callback(JSON.parse(employee));
      } else {
        callback(`Record does not exists ${error}`);
      }
    });
  },

  update: (model, file, employee, callback) => {
    try {
      fs.open(`${EmployeesModel.baseDir}${model}/${file}.json`, 'r+', (error, fileDescriptor) => {
        if (!error && fileDescriptor) {
          const stringData = JSON.stringify(employee);
          fs.ftruncate(fileDescriptor, (error) => {
            if (!error) {
              fs.writeFile(fileDescriptor, stringData, (error) => {
                if (!error) {
                  fs.close(fileDescriptor, (error) => {
                    if (!error) {
                      callback('Record updated');
                    } else {
                      callback('Error closing existing file');
                    }
                  });
                }
              });
            } else {
              callback('Error truncating file');
            }
          });
        }
      });
    } catch (e) {
      console.log('EmployeesModel update', e);
    }
  },

  delete: (model, file, callback) => {
    fs.unlink(`${EmployeesModel.baseDir}${model}/${file}.json`, (error) => {
      if (!error) {
        callback('deleted');
      } else {
        callback('Error deleting file');
      }
    });
  },


};


module.exports = EmployeesModel;
