const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

module.exports = function (passport) {
  var opts = {};
  opts.jwtFromRequest = ExtractJwt
    .fromExtractors(
      [
        ExtractJwt.fromAuthHeaderWithScheme("jwt"), 
        ExtractJwt.fromUrlQueryParameter('token')
      ]
    );
  opts.secretOrKey = 'secret';
  opts.ignoreExpiration = false;
  opts.expiresIn = 10;
  passport.use(new JwtStrategy(opts, function (jwt_payload, callback) {
    return callback(null, user);
  }));
};