const UtilityService = {
    queryStringToJSON: query => {
        var pairs = query.slice(0).split('&');

        var result = {};
        pairs.forEach(function (pair) {
            pair = pair.split('=');
            result[pair[0]] = decodeURIComponent(pair[1] || '');
        });

        return JSON.parse(JSON.stringify(result));
    }
};

module.exports = UtilityService;