const employeesModel = require('../models/employeesModel');

const EmployeesService = {
  model: 'employees',

  list: (data, callback) => {
    employeesModel.list('employees', (employees) => {
      callback(200, { employees });
    });
  },

  show: (data, callback) => {
    employeesModel.read(
      EmployeesService.model,
      data.uri.id,
      (employee) => {
        callback(200, { employee: employee });
      });
  },

  create: (data, callback) => {
    const new_id = Math.floor(Math.random() * Math.floor(9999999));
    employeesModel.create(
      EmployeesService.model,
      new_id,
      JSON.stringify(data.info),
      (error) => {
        if (!error) {
          callback(200, { id: new_id });
        } else {
          callback(200, { status });
        }
      });
  },

  update: (data, callback) => {
    employeesModel.update(
      EmployeesService.model,
      data.uri.id,
      data.info,
      (status) => {
        callback(200, { status });
      });
  },

  delete: (data, callback) => {
    employeesModel.delete(
      EmployeesService.model,
      data.uri.id,
      (status) => {
        callback(200, { status: status });
      });
  },

};

module.exports = EmployeesService;
