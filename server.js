const http = require('http');
const url = require('url');
const UrlPattern = require('url-pattern');
const { StringDecoder } = require('string_decoder');
const routes = require('./server/routes');
const utilityService = require('./server/services/utilityService');
const passport = require('passport');
require('./server/services/passportService')(passport);

const router = {
  'login': routes.login,
  'employees': routes.employees,
  'employees/:id': routes.getEmployees,
  'notFound': routes.notFound
};

const createServer = (req, res) => {
  const parsedUrl = url.parse(req.url, true);
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader('Access-Control-Allow-Methods', '*');
  res.setHeader("Access-Control-Allow-Headers", "*");
  const pathName = parsedUrl.pathname;
  const trimmedPath = pathName.replace(/^\/+|\/+$/g, '');
  const method = req.method.toLowerCase();
  const { headers } = req;
  const decoder = new StringDecoder('utf-8');
  let buffer = '';

  req.on('data', (data) => {
    buffer += decoder.write(data);
  });

  req.on('end', () => {
    buffer += decoder.end();

    const pattern = new UrlPattern(':resource/:id');
    const patternMatch = pattern.match(trimmedPath);
    let newPath = trimmedPath;

    if (patternMatch)
      newPath = `${patternMatch.resource}`;

    if (patternMatch && patternMatch.id)
      newPath = `${patternMatch.resource}/:id`;

    const chosenHandler = typeof (router[newPath]) !== 'undefined' ? router[newPath] : router.notFound;

    const data = {
      trimmedPath,
      uri: patternMatch,
      method,
      headers,
      payload: buffer,
    };

    chosenHandler(data, (code, load) => {
      const statusCode = typeof (code) === 'number' ? code : 200;
      const payload = typeof (load) === 'object' ? load : {};
      const payloadString = JSON.stringify(payload);
      res.writeHead(statusCode);
      res.end(payloadString);
    });
  });
};

const server = http.createServer(createServer);

server.listen(9000, () => {
  console.log('The server is listening on post 9000');
});
